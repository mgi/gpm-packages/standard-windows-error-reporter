## 0.1.4

Add optional parameter for error throttling interval to Create VI.

## 0.1.3

Use newer dependency (panel-actor-error-reporter 0.1.3)

## 0.1.2

Add optional parameter for error throttling interval to prevent multiple pop ups from overwhelming user.

## 0.1.1

Updated dependencies.

## 0.1.0

Initial Release.
